// polyfill for window performance now()
(function(){
 
  // prepare base perf object
  if (typeof window.performance === 'undefined') {
      window.performance = {};
  }
 
  if (!window.performance.now){
    
    var nowOffset = Date.now();
 
    if (performance.timing && performance.timing.navigationStart){
      nowOffset = performance.timing.navigationStart
    }
 
 
    window.performance.now = function now(){
      return Date.now() - nowOffset;
    }
 
  }
 
})();



window.onload = function() {

	var fps = 10,
		currentFrame = 0,
		totalFrames = 15,
		startTime = null,
		requestId = 0;


	var screen01 = document.querySelector('.screen-01'),
		screen02 = document.querySelector('.screen-02'),
		bg01 = document.querySelector('.bg-01'),
		bg02 = document.querySelector('.bg-02'),
		posX = posY = loopX = loopY = 0,
		fogAnimateFinished = false,
		fogInterval = null,
		seedImg = document.getElementById("seed_seq");

	// seed images
	var seedImgArr = new Array(15).join(',').split(',');
	seedImgArr = seedImgArr.map(function(el, i) {
		return 'img/seed/seed_' + ("000" + (i +1)).slice(-2) + '.png';
	});


	screen01.onclick = function() {
		bg02.className += ' fog-mask';
		bg02.style.display = 'block';
		bg01.style.display = 'block';

		// fogIntervalAnimate();

		totalFrames = 16;
		startFogAnimate();
	};

	function fogIntervalAnimate() {
		fogInterval = setInterval(function(){
			posX = loopX * -320;
			posY = loopY * -505;
			bg02.style['-webkit-mask-position'] = posX + "px " + posY + "px"; 
			loopX++;

			if (loopX >= 4) {
				loopX = 0
				loopY++;
			} 

			if (loopY >= 4) {
				loopX = loopY = 0;
				fogMaskAnimateFinished();
			}
		}, 100);
	}


	function fogMaskAnimateFinished() {
		clearInterval(fogInterval);
		// stopFogAnimate();
		screen01.style.display = 'none';
		screen02.style.display = 'block';
		screen02.className += ' shake';
	}


	function fogAnimateFrame(time) {

		var delta = (time - startTime) / 1000;
		currentFrame += (delta * fps);
		var frameNum = Math.floor(currentFrame);

		if (frameNum < 0) {
			currentFrame = frameNum = 0;
		}

		if (frameNum >= totalFrames) {
	    	currentFrame = frameNum = 0;
	  	}

		requestId = window.requestAnimationFrame(fogAnimateFrame);

		// animate
		var loopX = loopY = 0;
		loopX = frameNum % 4;
		loopY = parseInt(frameNum / 4);
		posX = loopX * -320;
		posY = loopY * -505;
		// console.log(frameNum);
		// console.log("X:" + loopX + " Y:" + loopY);
		// console.log("posX:" + posX + " posY:" + posY);
		bg02.style['-webkit-mask-position'] = posX + "px " + posY + "px";
		
		startTime = time;

		if (frameNum == totalFrames - 1)
			fogMaskAnimateFinished();
	}


	function startFogAnimate() {
		startTime = window.performance.now();
		equestId = window.requestAnimationFrame(fogAnimateFrame);
	}

	function stopFogAnimate() {
		if (requestId)
	    	window.cancelAnimationFrame(requestId);
	  	requestId = 0;
	}

	// document.getElementById('stopBtn').onclick = function() {
	// 	// stopFogAnimate();
	// 	// shakeEventDidOccur();
	// 	console.log('stopped');
	// }


	window.addEventListener('shake', shakeEventDidOccur, false);

	function shakeEventDidOccur () {
		if (screen02.className.indexOf('shake')) {
			// seedIntervalAnimate();
			seedAnimateStart();
		}
	}


	function seedIntervalAnimate() {
		var i = 0;
		setInterval(function() {
			seedImg.src = seedImgArr[i];
			i++;
			if (i >= 15) i = 0;
		}, 100);
	}

	function seedAmniateFrame(time) {
		var delta = (time - startTime) / 1000;
		currentFrame += (delta * fps);
		var frameNum = Math.floor(currentFrame);

		if (frameNum < 0) {
			currentFrame = frameNum = 0;
		}

		if (frameNum >= totalFrames) {
	    	currentFrame = frameNum = 0;
	  	}

		requestId = window.requestAnimationFrame(seedAmniateFrame);

		// animate
		var seedIndex = frameNum + 1
		seedImg.src = seedImgArr[frameNum];
		
		startTime = time;

		if (frameNum == totalFrames - 1)
			seedAnimateStop();
	}

	function seedAnimateStart() {
		fps = 10,
		currentFrame = 0,
		totalFrames = 15,
		requestId = 0;
		startTime = window.performance.now();

		equestId = window.requestAnimationFrame(seedAmniateFrame);
	}

	function seedAnimateStop() {
		if (requestId)
	    	window.cancelAnimationFrame(requestId);
	  	requestId = 0;
	}
};